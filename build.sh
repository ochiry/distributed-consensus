#!/bin/bash

asciidocfiles () {
  dist="public/${1#src/}/.."
  asciidoctor -a ext=html -r asciidoctor-diagram -D $dist $1
}
export -f asciidocfiles

find src -name "*.adoc" | xargs -I{} bash -c "asciidocfiles {}"
cp -r src/images public
