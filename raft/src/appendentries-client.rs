use std::{thread, time};

use components_raft::append_entries_client::AppendEntriesClient;
use components_raft::AppendEntriesAugument;

pub mod components_raft { // <- raft module
    tonic::include_proto!("raft"); // <- package
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = AppendEntriesClient::connect("http://[::1]:50051").await?;
    let one_second = time::Duration::from_millis(1000);
    for i in 1..3 {
        let request = tonic::Request::new(AppendEntriesAugument {
            term: i,
            leader_id: 2,
            prev_log_index: i-1,
            prev_log_term: i-1,
            entries: vec!["tonic".to_string()],
            leader_commit: 6,
        });
        
        let response = client.call_append_entries(request).await?;
        println!("{:?}", response);
        thread::sleep(one_second);
        
    }
    for i in 1..4 {
        let request = tonic::Request::new(AppendEntriesAugument {
            term: i,
            leader_id: 2,
            prev_log_index: i-1,
            prev_log_term: i-1,
            entries: vec!["tanic".to_string()],
            leader_commit: 6,
        });
        
        let response = client.call_append_entries(request).await?;
        println!("{:?}", response);
        thread::sleep(one_second);
        
    }

    // println!("RESPONSE={:?}", response);

    Ok(())
}
