use std::cmp;

use std::sync::{Arc, Mutex};

use tonic::{transport::Server, Request, Response, Status};

use raft_components::append_entries_server::{AppendEntries, AppendEntriesServer};
use raft_components::{AppendEntriesAugument, AppendEntriesResult};

pub mod raft_components {
    // <- raft module
    tonic::include_proto!("raft"); // <- package
}

// #[derive(Clone)]
#[derive(Debug)]
struct LogEntry(u64, String);

struct RawState {
    current_term: u64,
    voted_for: u32,
    log: Vec<LogEntry>,
    commit_index: u64,
    last_applied: u64,
    next_index: Vec<u64>,
    match_index: Vec<u64>,
}

impl Default for RawState {
    fn default() -> Self {
        Self {
            current_term: 0,
            voted_for: 0,
            log: vec![LogEntry(0, "".to_string())],
            commit_index: 0,
            last_applied: 0,
            next_index: Vec::new(),
            match_index: Vec::new(),
        }
    }
}

pub struct State {
    arced_raw_state: Arc<Mutex<RawState>>,
}

impl Default for State {
    fn default() -> Self {
        Self {
            arced_raw_state: Arc::new(Mutex::new(Default::default())),
        }
    }
}

fn make_result(term: u64, success: bool) -> Result<Response<AppendEntriesResult>, Status> {
    let reply = AppendEntriesResult {
        term: term,
        success: success,
    };
    // println!("make resulet {}", success);
    return Ok(Response::new(reply));
}

#[tonic::async_trait]
impl AppendEntries for State {
    // Arguments:
    // term: leader’s term
    // leaderId: so follower can redirect clients
    // prevLogIndex: index of log entry immediately precedingnew ones
    // prevLogTerm: term of prevLogIndex entry
    // entries[]: log entries to store (empty for heartbeat;may send more than one for efficiency)
    // leaderCommit: leader’s commitIndex
    // Results:
    // term: currentTerm, for leader to update itself
    // success: true if follower contained entry matchingprevLogIndex and prevLogTer
    async fn call_append_entries(
        &self,
        request: Request<AppendEntriesAugument>,
    ) -> Result<Response<AppendEntriesResult>, Status> {
        println!("Got a request from {:?}", request.remote_addr());
        println!("request = {:?}", request);
        // 1. Reply false if term < currentTerm
        let arced_raw_state = &self.arced_raw_state;
        let msg = request.get_ref();
        if msg.term < arced_raw_state.lock().unwrap().current_term {
            return make_result(request.into_inner().term, false);
        }
        println!("Step 1 finished");
        // 2. Reply false if log doesn't contain an entry at prevLogIndex whose term matches prevLogTerm
        // there exists log 0-th index. #log = 1+?
        if let Some(term) = arced_raw_state
            .lock()
            .unwrap()
            .log
            .get(request.get_ref().prev_log_index as usize)
        {
            if term.0 != request.get_ref().prev_log_term {
                return make_result(arced_raw_state.lock().unwrap().current_term, false);
            }
        } else {
            return make_result(arced_raw_state.lock().unwrap().current_term, false);
        }

        // 3. If an existing entry conflicts with a new one (same index but different terms), delete the existing entry and all that follow it.
        let mut entry_correctness = true;
        let mut next_empty = false;
        let start_entry_index = (request.get_ref().prev_log_index as usize) + 1;
        let mut _entry_size = request.get_ref().entries.len();
        // Check a space which entry dominate
        if arced_raw_state.lock().unwrap().log.len() > start_entry_index + _entry_size {
            entry_correctness = false;
        }
        if arced_raw_state.lock().unwrap().log.len()
            == (request.get_ref().prev_log_index as usize) + 1
        {
            next_empty = true;
        }

        if !next_empty && entry_correctness {
            for i in 0.._entry_size {
                println!(
                    "{} {:?}",
                    start_entry_index,
                    arced_raw_state.lock().unwrap().log
                );
                if arced_raw_state
                    .lock()
                    .unwrap()
                    .log
                    .get(start_entry_index + i)
                    .unwrap()
                    .1
                    != *request.get_ref().entries.get(i).unwrap()
                {
                    entry_correctness = false;
                    break;
                }
            }
        }
        if !next_empty && !entry_correctness {
            // a..b = a..a if a > b
            for _ in start_entry_index..arced_raw_state.lock().unwrap().log.len() {
                arced_raw_state.lock().unwrap().log.pop();
            }
            next_empty = true;
        }
        // Step 4. Append any new entries not already in the log
        if next_empty {
            // let term = request.into_inner().term;
            // let entries = request.into_inner().entries;
            // request.get_ref().entries.reverse();
            for i in 0.._entry_size {
                println!("{} {:?}", i, request.get_ref().entries[i]);
                arced_raw_state.lock().unwrap().log.push(LogEntry(
                    request.get_ref().term,
                    request.get_ref().entries[i].clone(),
                ));
            }
        }
        
        println!("{:?}", arced_raw_state.lock().unwrap().log);

        // Step 5. If leaderCommit > commitIndex, set commitIndex = min(leaderCommit, index of the last new entry)
        if msg.leader_commit > arced_raw_state.lock().unwrap().current_term {
            arced_raw_state.lock().unwrap().current_term = cmp::max(msg.leader_commit, arced_raw_state.lock().unwrap().commit_index-1);
        }
        let reply = AppendEntriesResult {
            term: arced_raw_state.lock().unwrap().current_term,
            success: entry_correctness,
        };
        println!("reply {:?}", reply);

        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse().unwrap();
    // let greeter = State::default();
    let state: State = Default::default();

    println!("GreeterServer listening on {}", addr);

    Server::builder()
        .add_service(AppendEntriesServer::new(state))
        .serve(addr)
        .await?;

    Ok(())
}
