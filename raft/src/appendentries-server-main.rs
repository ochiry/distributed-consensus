use tonic::{transport::Server};
use raft::state::State;
use raft::state::raft_components::append_entries_server::AppendEntriesServer;

// use raft_component::append_entries_server::{AppendEntriesServer};

// pub mod raft_component {
//     // <- raft module
//     tonic::include_proto!("raft"); // <- package
// }


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse().unwrap();
    // let greeter = State::default();
    let state: State = Default::default();

    println!("GreeterServer listening on {}", addr);

    Server::builder()
        .add_service(AppendEntriesServer::new(state))
        .serve(addr)
        .await?;

    Ok(())
}
